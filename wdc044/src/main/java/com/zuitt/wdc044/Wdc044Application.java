package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication

@RestController
public class Wdc044Application {

	public static void main(String[] args) {
//		This method starts the whole Spring Framework
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name){
		return String.format("Hello %s!", name);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String name) {
		return String.format("Hi %s!", name);
	}

	@GetMapping("/nameAge")
	public String nameAge(@RequestParam(value="name", defaultValue = "Juan") String name,
	@RequestParam(value="age", defaultValue = "18") Integer age){
		return String.format("Hello %s! Your age is %d", name, age);
	}
}
